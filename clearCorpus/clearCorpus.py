import sys
import re

def checkAlphabet(line):

	sr, en = line.split("\t")

	if sr == en:
		return False

	if re.search("[^"
		" .?!,'()"
		"АБВГДЂЕЖЗИЈКЛЉМНЊОПРСТЋУФХЦЧЏШабвгдђежзијклљмнњопрстћуфхцчџш"
		"ABCČĆDĐEFGHIJKLMNOPRSŠTUVZŽabcčćdđefghijklmnoprsštuvzž"
		"]", sr) is not None:

		return False

	if re.search("[^"
		" .?!,'()"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
		"]", en) is not None:
		
		return False

	return True


def modifyAlphabet(line):

	line = re.sub("А", "a", line)
	line = re.sub("Б", "b", line)
	line = re.sub("В", "v", line)
	line = re.sub("Г", "g", line)
	line = re.sub("Д", "d", line)
	line = re.sub("Ђ", "dx", line)
	line = re.sub("Е", "e", line)
	line = re.sub("Ж", "zx", line)
	line = re.sub("З", "z", line)
	line = re.sub("И", "i", line)
	line = re.sub("Ј", "j", line)
	line = re.sub("К", "k", line)
	line = re.sub("Л", "l", line)
	line = re.sub("Љ", "lx", line)
	line = re.sub("М", "m", line)
	line = re.sub("Н", "n", line)
	line = re.sub("Њ", "nx", line)
	line = re.sub("О", "o", line)
	line = re.sub("П", "p", line)
	line = re.sub("Р", "r", line)
	line = re.sub("С", "s", line)
	line = re.sub("Т", "t", line)
	line = re.sub("Ћ", "cx", line)
	line = re.sub("У", "u", line)
	line = re.sub("Ф", "f", line)
	line = re.sub("Х", "h", line)
	line = re.sub("Ц", "c", line)
	line = re.sub("Ч", "cy", line)
	line = re.sub("Џ", "dy", line)
	line = re.sub("Ш", "sx", line)

	line = re.sub("а", "a", line)
	line = re.sub("б", "b", line)
	line = re.sub("в", "v", line)
	line = re.sub("г", "g", line)
	line = re.sub("д", "d", line)
	line = re.sub("ђ", "dx", line)
	line = re.sub("е", "e", line)
	line = re.sub("ж", "zx", line)
	line = re.sub("з", "z", line)
	line = re.sub("и", "i", line)
	line = re.sub("ј", "j", line)
	line = re.sub("к", "k", line)
	line = re.sub("л", "l", line)
	line = re.sub("љ", "lx", line)
	line = re.sub("м", "m", line)
	line = re.sub("н", "n", line)
	line = re.sub("њ", "nx", line)
	line = re.sub("о", "o", line)
	line = re.sub("п", "p", line)
	line = re.sub("р", "r", line)
	line = re.sub("с", "s", line)
	line = re.sub("т", "t", line)
	line = re.sub("ћ", "cx", line)
	line = re.sub("у", "u", line)
	line = re.sub("ф", "f", line)
	line = re.sub("х", "h", line)
	line = re.sub("ц", "c", line)
	line = re.sub("ч", "cy", line)
	line = re.sub("џ", "dy", line)
	line = re.sub("ш", "sx", line)

	line = re.sub("Dž", "dy", line)
	line = re.sub("Đ", "dx", line)
	line = re.sub("Ž", "zx", line)
	line = re.sub("Lj", "lx", line)
	line = re.sub("Nj", "nx", line)
	line = re.sub("Ć", "cx", line)
	line = re.sub("Č", "cy", line)
	line = re.sub("Š", "sx", line)

	line = re.sub("dž", "dy", line)
	line = re.sub("đ", "dx", line)
	line = re.sub("ž", "zx", line)
	line = re.sub("lj", "lx", line)
	line = re.sub("nj", "nx", line)
	line = re.sub("ć", "cx", line)
	line = re.sub("č", "cy", line)
	line = re.sub("š", "sx", line)

	line = re.sub("A", "a", line)
	line = re.sub("B", "b", line)
	line = re.sub("V", "v", line)
	line = re.sub("G", "g", line)
	line = re.sub("D", "d", line)
	line = re.sub("E", "e", line)
	line = re.sub("Z", "z", line)
	line = re.sub("I", "i", line)
	line = re.sub("J", "j", line)
	line = re.sub("K", "k", line)
	line = re.sub("L", "l", line)
	line = re.sub("M", "m", line)
	line = re.sub("N", "n", line)
	line = re.sub("O", "o", line)
	line = re.sub("P", "p", line)
	line = re.sub("R", "r", line)
	line = re.sub("S", "s", line)
	line = re.sub("T", "t", line)
	line = re.sub("U", "u", line)
	line = re.sub("F", "f", line)
	line = re.sub("H", "h", line)
	line = re.sub("C", "c", line)

	line = re.sub("X", "x", line)
	line = re.sub("Y", "y", line)
	line = re.sub("Q", "q", line)
	line = re.sub("W", "w", line)

	return line

def modifySyntax(line):

	line = re.sub("\([^)]*\)", "", line)
	line = re.sub("[,']", " ", line)

	line = re.sub("\.", " .", line)
	line = re.sub("\?", " ?", line)
	line = re.sub("\!", " !", line)
	
	line = re.sub(" *\t *", "\t", line)
	line = re.sub(" +", " ", line)

	line = line.strip()

	return line


def checkSyntax(line):

	if re.search("(^[a-z ]+\.\t[a-z ]+\.$"
		     "|^[a-z ]+\?\t[a-z ]+\?$"
		     "|^[a-z ]+!\t[a-z ]+!$)", line) is None: 
		return False

	return True


def checkDuplicates(lines):

    return list( set(lines) )
	

def checkLength(line, n):
	
	sr, en = line.split("\t")
	
	if len( sr.split() ) > n or len( en.split() ) > n :
		return False

	return True


def countWords(lines):

	counts = dict()
	words = " ".join(lines).split()

	for word in words:
		if word in counts:
			counts[word] += 1
		else:
			counts[word] = 1

	return counts


def checkLexic1(lines):

	clear = []
	again = True

	while again:

		again = False
		counts = countWords(lines)

		for line in lines:
			t = True
			for word in line.split():
				if counts[word] == 1:
					t = False
					again = True
					break
			if t:
				clear.append(line)

		lines = clear
		clear = []

	return lines


def checkLexic2(lines):

	counts = countWords(lines)
	clear = []

	for line in lines:

		newLine = ''

		for word in line.split():
			if newLine != '':
				if newLine[-1] in '.?!':
					newLine = newLine + '\t'
				else:
					newLine = newLine + ' '

			if counts[word] == 2:
				newLine = newLine + '<UNK>'
			else:
				newLine = newLine + word

		clear.append(newLine)

	return clear


lines = open(sys.argv[1], encoding='utf-8').read().strip().split('\n')

print('Lines original: ' + str(len(lines)) )

res = []
for line in lines:

	if not checkAlphabet(line):
		continue
	line=modifyAlphabet(line)
	line=modifySyntax(line)
	if not checkSyntax(line):
		continue

	res.append(line)

lines = checkDuplicates( res )


res10 = []
res20 = []
res40 = []
for line in lines:

	if checkLength(line, 10 ):
		res10.append(line)
	if checkLength(line, 20 ):
		res20.append(line)
	if checkLength(line, 40 ):
		res40.append(line)


res10 = checkLexic1(res10);
res20 = checkLexic1(res20);
res40 = checkLexic1(res40);

res10 = checkLexic2(res10);
res20 = checkLexic2(res20);
res40 = checkLexic2(res40);

res10 = checkDuplicates( res10 )
res20 = checkDuplicates( res20 )
res40 = checkDuplicates( res40 )


print('Lines 10: ' + str(len(res10)) )
print('Lines 20: ' + str(len(res20)) )
print('Lines 40: ' + str(len(res40)) )

print('Words 10: ' + str(len(countWords(res10))) )
print('Words 20: ' + str(len(countWords(res20))) )
print('Words 40: ' + str(len(countWords(res40))) )



f = open("10"+sys.argv[2], 'w')
for line in res10:	
	f.write(line+'\n')

f = open("20"+sys.argv[2], 'w')
for line in res20:	
	f.write(line+'\n')

f = open("40"+sys.argv[2], 'w')
for line in res40:	
	f.write(line+'\n')

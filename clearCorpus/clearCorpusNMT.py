from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.bleu_score import SmoothingFunction
import re


def modifyAlphabet(line):

	line = re.sub("А", "a", line)
	line = re.sub("Б", "b", line)
	line = re.sub("В", "v", line)
	line = re.sub("Г", "g", line)
	line = re.sub("Д", "d", line)
	line = re.sub("Ђ", "dx", line)
	line = re.sub("Е", "e", line)
	line = re.sub("Ж", "zx", line)
	line = re.sub("З", "z", line)
	line = re.sub("И", "i", line)
	line = re.sub("Ј", "j", line)
	line = re.sub("К", "k", line)
	line = re.sub("Л", "l", line)
	line = re.sub("Љ", "lx", line)
	line = re.sub("М", "m", line)
	line = re.sub("Н", "n", line)
	line = re.sub("Њ", "nx", line)
	line = re.sub("О", "o", line)
	line = re.sub("П", "p", line)
	line = re.sub("Р", "r", line)
	line = re.sub("С", "s", line)
	line = re.sub("Т", "t", line)
	line = re.sub("Ћ", "cx", line)
	line = re.sub("У", "u", line)
	line = re.sub("Ф", "f", line)
	line = re.sub("Х", "h", line)
	line = re.sub("Ц", "c", line)
	line = re.sub("Ч", "cy", line)
	line = re.sub("Џ", "dy", line)
	line = re.sub("Ш", "sx", line)

	line = re.sub("а", "a", line)
	line = re.sub("б", "b", line)
	line = re.sub("в", "v", line)
	line = re.sub("г", "g", line)
	line = re.sub("д", "d", line)
	line = re.sub("ђ", "dx", line)
	line = re.sub("е", "e", line)
	line = re.sub("ж", "zx", line)
	line = re.sub("з", "z", line)
	line = re.sub("и", "i", line)
	line = re.sub("ј", "j", line)
	line = re.sub("к", "k", line)
	line = re.sub("л", "l", line)
	line = re.sub("љ", "lx", line)
	line = re.sub("м", "m", line)
	line = re.sub("н", "n", line)
	line = re.sub("њ", "nx", line)
	line = re.sub("о", "o", line)
	line = re.sub("п", "p", line)
	line = re.sub("р", "r", line)
	line = re.sub("с", "s", line)
	line = re.sub("т", "t", line)
	line = re.sub("ћ", "cx", line)
	line = re.sub("у", "u", line)
	line = re.sub("ф", "f", line)
	line = re.sub("х", "h", line)
	line = re.sub("ц", "c", line)
	line = re.sub("ч", "cy", line)
	line = re.sub("џ", "dy", line)
	line = re.sub("ш", "sx", line)

	line = re.sub("Dž", "dy", line)
	line = re.sub("Đ", "dx", line)
	line = re.sub("Ž", "zx", line)
	line = re.sub("Lj", "lx", line)
	line = re.sub("Nj", "nx", line)
	line = re.sub("Ć", "cx", line)
	line = re.sub("Č", "cy", line)
	line = re.sub("Š", "sx", line)

	line = re.sub("dž", "dy", line)
	line = re.sub("đ", "dx", line)
	line = re.sub("ž", "zx", line)
	line = re.sub("lj", "lx", line)
	line = re.sub("nj", "nx", line)
	line = re.sub("ć", "cx", line)
	line = re.sub("č", "cy", line)
	line = re.sub("š", "sx", line)

	line = re.sub("A", "a", line)
	line = re.sub("B", "b", line)
	line = re.sub("V", "v", line)
	line = re.sub("G", "g", line)
	line = re.sub("D", "d", line)
	line = re.sub("E", "e", line)
	line = re.sub("Z", "z", line)
	line = re.sub("I", "i", line)
	line = re.sub("J", "j", line)
	line = re.sub("K", "k", line)
	line = re.sub("L", "l", line)
	line = re.sub("M", "m", line)
	line = re.sub("N", "n", line)
	line = re.sub("O", "o", line)
	line = re.sub("P", "p", line)
	line = re.sub("R", "r", line)
	line = re.sub("S", "s", line)
	line = re.sub("T", "t", line)
	line = re.sub("U", "u", line)
	line = re.sub("F", "f", line)
	line = re.sub("H", "h", line)
	line = re.sub("C", "c", line)

	line = re.sub("X", "x", line)
	line = re.sub("Y", "y", line)
	line = re.sub("Q", "q", line)
	line = re.sub("W", "w", line)

	return line

def modifySyntax(line):

	line = re.sub("\([^)]*\)", "", line)
	line = re.sub("[,']", " ", line)

	line = re.sub("\.", " .", line)
	line = re.sub("\?", " ?", line)
	line = re.sub("\!", " !", line)
	
	line = re.sub(" *\t *", "\t", line)
	line = re.sub(" +", " ", line)

	line = re.sub("<unk>", "<UNK>", line)

	line = line.strip()

	return line



lines = open("nmt.txt", encoding='utf-8').read().strip().split('\n')

hypotheses = []
for line in lines:

	line=modifyAlphabet(line)
	line=modifySyntax(line)

	hypotheses.append(line.split())


lines = open("sr-enClear.txt", encoding='ascii').read().strip().split('\n')


smoothie = SmoothingFunction().method4
f = open('sr-enNMTClear.txt', 'w')
br = 0
for i in range(len(lines)):

	br = br + 1
	if br % 10000 == 0:
		print(br)

	references = [ lines[i].split("\t")[0].split() ]
	
	try:
		if sentence_bleu( references, hypotheses[i], smoothing_function=smoothie ) > 0.15 :
			f.write(lines[i]+'\n')

	except ZeroDivisionError:
		    print('ZeroDivisonError!')

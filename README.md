# English-Serbian NMT System

This repository is a collection of items I created towards the completion of my master thesis at the Department of Mathematics and Informatics, Faculty of Science, University of Kragujevac.

This repository contains scripts for cleaning corpora and training NMT model based on LSTM for translating from English to Serbian.

## Structure of this Repository

- `clearCorpus` folder contains scripts for cleaning corpora.
- `nmt2_128_16.py` file is a Pytorch script for training the NMT model with a depth of 2 layers on the encoder and decoder, a 128 batch size and for stopping training on 16 epochs without improvement in BLEU score.

## Corpora

Parallel corpora can be found on the [Opus project](http://opus.nlpl.eu/). Files with English and Serbian translations should be merged with tab in one file. For example, the prepared file should be in this format:

```
Hajde da probamo nešto.	Let's try something.
Морам да идем да спавам.	I have to go to sleep.
Danas je 18. Juni i Mjurijelov je rođendan.	Today is June 18th and it is Muiriel's birthday!
Mjurijelu je sada 20.	Muiriel is 20 now.
Шифра је "Мјуриел".	The password is "Muiriel".
Šifra je "Mjurijel".	The password is "Muiriel".
Vraćam se uskoro.	I will be back soon.
Ovo se nikada neće završiti.	This is never going to end.
Prosto ne znam šta da kažem.	I just don't know what to say.
To je bio zli zec.	That was an evil bunny.
Bio sam u planinama.	I was in the mountains.
```

## Hyperparameters


| Hyperparameter | Value |
| ------ | ------ |
| RNN type | LSTM |
| Embedding size | 512 |
| Context vector size | 512 |
| Encoder type  | bidirectional |
| Encoder depth | 2 |
| Decoder depth | 2 |
| Attention type | global |
| Loss function | NLLLoss |
| Batch size | 128 |
| Dropout | 0.2 |
| Optimizer | Adam |
| Learning rate | 0.0002 |
| Stopping criterion | 16 epochs without improvement in BLEU score |

## Testing environment

The scripts were run in the following environment:

| Software | Version |
| ------ | ------ |
| Python | 3.8 |
| PyTorch | 1.5 |
| NVIDIA driver | 440.82 |
| CUDA driver | 10.2 |

| Hardware | 
| ------ | 
| NVIDIA TITAN Xp GPU 12 GB| 
| Intel Core i5-6400 CPU @ 2.70GHz | 
| 8GB RAM | 

## Results

Test set results for Tatoeba parallel corpus after NMT cleaning:

|  | BLEU 1 (%) | BLEU 2 (%) | BLEU 3 (%) | BLEU 4 (%) |
| ------ | ------ | ------ | ------ | ------ |
| Google Translate | 71.13 | 57.84 | 46.42 | 38.15 |
| Microsoft Translator | 68.09 | 51.41 | 41.24 | 33.82 |
| Yandex Translate | 56.40 | 37.93 | 27.62 | 20.52 |
| Experimental model | 71.11 | 54.50 | 43.54 | 35.47 |

## How to start project

Random sample lines:

`chmod +x randomSampler.sh`
`./randomSampler.sh 0.001 sr-enOriginal.txt`

where `0.001` is sample rate and `sr-enOriginal.txt` is text file.

Cleaning corpora:

`python clearCorpus.py sr-enOriginal.txt sr-enCrear.txt`

where `sr-enOriginal.txt` is input file with parallel corpus where translations are separated with tab, and `sr-enCrear.txt` is resulting file. Console output will be like this:

```
Lines original: 13513
Lines 10: 6335
Lines 20: 6912
Lines 40: 6918
Words 10: 2518
Words 20: 2755
Words 40: 2759
```

NMT cleaning corpora:

`python clearCorpusNMT.py`

where you must put clear corpus in file `sr-enCrear.txt` (result file of `clearCorpus` script) and NMT traslated sentences separated with new lines in `nmt.txt` in current directory. Result will be saved in file `sr-enNMTClear.txt`. Console output contains number of cleared lines on every 1000 lines.


Train model:

`python nmt2_128_16.py`

where in same directory you must put sr-en.txt file that contains parallel corpus. After every epoch console output be like that:

```
Iter: 72 
Leaning Rate: 0.0001 
Time: 0h 27m 51s 
Train Loss: 0.1130286715329046 
Test Loss: 2.1844388613511905 

 				> that s really beautiful . 
 				= to je zaista prelepo . 
 				< to je zaista lepo . <EOS> 

 				> i m the only one who can do that . 
 				= ja sam jedini koji mozxe to . 
 				< ja sam jedina koja mozxe to . <EOS> 

 				> the best way to lose weight is to eat less and exercise more . 
 				= najbolxi nacyin da <UNK> kilazxu je da jedete manxe i vezxbate visxe . 
 				< najbolxi nacyin da izgubisx kilazxu je da manxe jedesx i visxe vezxbasx . <EOS> 

 				> i have a train to catch . 
 				= treba da <UNK> voz . 
 				< moram da <UNK> voz . <EOS> 

 				> tom was really mad . 
 				= tom je zaista bio besan . 
 				< tom je bio zaista <UNK> . <EOS> 

 				> come here . 
 				= dodxi ovamo . 
 				< dodxi ovde . <EOS> 

 				> i m sure you understand . 
 				= sigurna sam da cxesx razumeti . 
 				< siguran sam da cxesx razumeti . <EOS> 

 				> it looked yellow to me . 
 				= meni je izgledalo zxuto . 
 				< izgledalo mi je zxuto . <EOS> 

 				> you ll probably never see me again . 
 				= verovatno me nikada visxe necxesx videti . 
 				< verovatno me nikad visxe necxesx videti . <EOS> 

 				> i m in better shape than you are . 
 				= u bolxoj sam formi od tebe . 
 				< u bolxoj sam formi nego ti . <EOS> 

 				> never say never . 
 				= nikad ne reci nikad . 
 				< nikad ne nikad nisxta . <EOS> 

 				> tell me why you did it . 
 				= kazxi mi zasxto si uradio to . 
 				< reci mi zasxto si uradila to . <EOS> 

 				> i m a bit worried now . 
 				= sada sam pomalo zabrinut . 
 				< sad sam pomalo zabrinuta . <EOS> 

 				> i m very proud of that . 
 				= veoma sam ponosan na to . 
 				< veoma sam ponosna na to . <EOS> 

 				> you run . 
 				= vi trcyite . 
 				< ti trcyisx . <EOS> 

 				> these are my best friends . 
 				= ovo su moji najbolxi drugari . 
 				< <UNK> su mi najbolxi prijatelxi . <EOS> 

 				> don t say a word . 
 				= nikom ni recy . 
 				< ne nemoj da se <UNK> . <EOS> 

 				> my father doesn t like football . 
 				= moj otac ne voli fudbal . 
 				< moj otac ne voli fudbal . <EOS> 

 				> tom never told me what he wanted to eat . 
 				= tom mi nikad nije rekao sxta zxeli da jede . 
 				< tom mi je rekao da mu sxta je zxeleo . <EOS> 

 				> you re working too hard . 
 				= radisx previsxe naporno . 
 				< danas izgledasx previsxe previsxe . <EOS> 

 				> i couldn t help feeling sorry for him . 
 				= nisam mogla a da ga ne <UNK> . 
 				< nisam mogao a da ga ne <UNK> . <EOS> 

 				> i just got here yesterday . 
 				= tek sam stigla ovde jucye . 
 				< tek sam stigao ovde jucye . <EOS> 

 				> if there is no <UNK> of it <UNK> will be made of it . 
 				= ako ne postoji <UNK> sa tim <UNK> cxe biti napravlxen sa tim . 
 				< ne zxelisx da se <UNK> od sve sxto je potrebno . <EOS> 

 				> i had never seen anything like that before . 
 				= nikad pre nisam video nesxto tako . 
 				< nikada nisam video nisxta o tome . <EOS> 

 				> we failed . 
 				= nismo uspeli . 
 				< <UNK> smo . <EOS> 

 				> i m not a bad student . 
 				= nisam losx student . 
 				< nisam losx koji . <EOS> 

 				> i don t think tom will give you the keys . 
 				= sumnxam da cxe tom da ti da klxucyeve . 
 				< ne znam da li tom nam cxe da radisx . <EOS> 

 				> this tea <UNK> good . 
 				= ovaj cyaj lepo <UNK> . 
 				< ovo su dobro <UNK> . <EOS> 

 				> he couldn t understand much of what they said to each other . 
 				= nije mogao da razume nesxto mnogo od <UNK> sxta su oni rekli jedno drugom . 
 				< nije mogao da <UNK> sxta je rekao da ne govori . <EOS> 

 				> have you ever written a computer program ? 
 				= da li si ikad napisala program za racyunar ? 
 				< jesi li ikad napisao program za racyunar ? <EOS> 

 				> let me try another one . 
 				= daj da probam drugu . 
 				< daj da probam drugog . <EOS> 

 				> when will the italian <UNK> start ? 
 				= kada cxe krenuti <UNK> <UNK> jezika ? 
 				< kada cxe krenuti <UNK> <UNK> jezika ? <EOS> 

 				> tom wanted to be good . 
 				= tom je hteo da bude dobar . 
 				< tom je zxeleo da bude dobro . <EOS> 

 				> he committed suicide . 
 				= izvrsxio je samoubistvo . 
 				< on je hteo da <UNK> . <EOS> 

 				> just look at all this . 
 				= samo pogledaj sve ovo . 
 				< samo pogledaj <UNK> . <EOS> 

 				> i m glad that tom won . 
 				= drago mi je sxto je tom pobedio . 
 				< drago mi je da je tom pobedio . <EOS> 

 				> i remember everything . 
 				= secxam se svega . 
 				< sve sam sve . <EOS> 

 				> it would be helpful if you could do that for me . 
 				= znacyilo bi mi ako bi to mogao da mi uradisx . 
 				< bilo bi ako bi mogao da bilo to uradi . <EOS> 

 				> get serious ! 
 				= <UNK> se ! 
 				< <UNK> se ! <EOS> 

 				> are those my glasses ? 
 				= jesu li to moje <UNK> ? 
 				< da li su to moje <UNK> ? <EOS> 

 				> i couldn t help feeling sorry for him . 
 				= nisam mogla a da ga ne <UNK> . 
 				< nisam mogao a da ga ne <UNK> . <EOS> 

 				> have you told tom this ? 
 				= jesi li rekla tomu ovo ? 
 				< da li si rekla tomu ovo ? <EOS> 

 				> i m not a <UNK> man . 
 				= nisam bogat cyovek . 
 				< ja nisam bogat cyovek . <EOS> 

 				> i think that helped me . 
 				= mislim da mi je to pomoglo . 
 				< mislim da to mi je to . <EOS> 

 				> i wish i could paint like that . 
 				= voleo bih da znam da slikam tako . 
 				< voleo bih da mogu da slikam tako . <EOS> 

 				> have you ever been there ? 
 				= da li si ikad bio ovde ? 
 				< da li si bio ovde ranije ? <EOS> 

 				> come on back here . 
 				= dodxi ovamo <UNK> . 
 				< dodxi ovamo se ovde . <EOS> 

 				> i m not pretty . 
 				= nisam lep . 
 				< nisam lepa . <EOS> 

 				> i need a sincere girlfriend . 
 				= treba mi iskrena devojka . 
 				< potrebna mi je iskrena devojka . <EOS> 

 				> i bought lots of stuff . 
 				= kupio sam puno stvari . 
 				< kupio sam mnogo stvari . <EOS> 
Train perplexity: 1.1196640349725975
Test perplexity: 8.885661066321834
Test BLEU 1: 0.6844672031211899
Test BLEU 2: 0.5222865723418434
Test BLEU 3: 0.4096514572342442
Test BLEU 4: 0.32666530817778383 
MAX BLEU 4: 0.3454201265549573
Without improvement: 3 epochs
```

Script will crate folder like that: `SREN_directions.2_layers.2_hidden.512_dropout.0.2_learningrate.0.0002_batch.128_epochs.1000` and that folder contains pictures for tracking training process, `log.txt` file, `enc_weights.pt` and `dec_weights.pt` that contains trained parameters for encoder and decoder, and files that contains training statistic. 

You can change model parameter in script:

```
"""PROVIDE INFORMATION ON THE DATASET AND DATASET CLEANING"""

input_lang_name = 'en'
output_lang_name = 'sr'

"""name of your dataset"""
dataset = 'clear'

"""file path of dataset in the form of a tuple. If translated sentences are
stored in two files, this tuple will have two elements"""
raw_data_file_path = ('sr-en.txt',)

"""True if you want to reverse the order of the sentence pairs. For example, 
in our dataset the sentence pairs list the English sentence first followed by
the French translation. But we want to translate from French to English,
so we set reverse as True."""
reverse=True

"""Remove sentences from dataset that are longer than trim (in either language)"""
trim = 50

"""max number of words in the vocabulary for both languages"""
max_vocab_size= 100000

"""if true removes sentences from the dataset that don't start with eng_prefixes.
Typically will want to use False, but implemented to compare results with Pytorch
tutorial. Can also change the eng_prefixes to prefixes of other languages or
other English prefixes. Just be sure that the prefixes apply to the OUTPUT
language (i.e. the language that the model is translating to NOT from)"""
start_filter = False

"""denotes what percentage of the data to use as training data. the remaining 
percentage becomes test data. Typically want to use 0.8-0.9. 1.0 used here to 
compare with PyTorch results where no test set was utilized"""
perc_train_set = 0.9

"""OUTPUT OPTIONS"""

"""denotes how often to evaluate a loss on the test set and print
sample predictions on the test set.
if no test set, simply prints sample predictions on the train set."""
test_eval_every = 1

"""denotes how often to plot the loss values of train and test (if applicable)"""
plot_every = 1

"""if true creates a txt file of the output"""
create_txt = True

"""if true saves the encoder and decoder weights to seperate .pt files for later use"""
save_weights = True

"""HYPERPARAMETERS: FEEL FREE TO PLAY WITH THESE TO TRY TO ACHIEVE BETTER RESULTS"""

"""signifies whether the Encoder and Decoder should be bidirectional LSTMs or not"""
bidirectional = True
if bidirectional:
    directions = 2
else:
    directions = 1

"""number of layers in both the Encoder and Decoder"""
layers = 2

"""Hidden size of the Encoder and Decoder"""
hidden_size = 512

"""Dropout value for Encoder and Decoder"""
dropout = 0.2

"""Training set batch size"""
batch_size = 128

"""Test set batch size"""
test_batch_size = 128

"""number of epochs (full passes through the training data)"""
epochs = 1000

"""Initial learning rate"""
learning_rate= 0.0002

"""Learning rate schedule. Signifies by what factor to divide the learning rate
at a certain epoch. For example {5:10} would divide the learning rate by 10
before the 5th epoch and {5:10, 10:100} would divide the learning rate by 10
before the 5th epoch and then again by 100 before the 10th epoch"""
lr_schedule = {}

"""loss criterion, see https://pytorch.org/docs/stable/nn.html for other options"""
criterion = nn.NLLLoss()

"""Start new train or load models"""
newTrain = True

```




## Related Projects

- [NLP From Scratch: Translation with a Sequence to Sequence Network and Attention](https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html) 
- [nmt_tutorial](https://github.com/qlanners/nmt_tutorial)

